/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//alert('init');
$( document ).ready(function() {
    load_settings();

    $('#btn').click(function(e){
        //alert('save');
        e.preventDefault();
        save_settings();
    });
});


function save_settings () 
{
    localStorage.setItem('background_color', $('#background_color').val());
    localStorage.setItem('text_color', $('#text_color').val());
    localStorage.setItem('text_size', $('#text_size').val());
    
    apply_preferences_to_page();
}

function load_settings () 
{
    var background_color = localStorage.getItem('background_color');
    var text_color = localStorage.getItem('text_color');
    var text_size = localStorage.getItem('text_size');
    
    $('#background_color').val(background_color);
    $('#text_color').val(text_color);  
    $('#text_size').val(text_size)

    apply_preferences_to_page();  
}

function apply_preferences_to_page()
{
    $('body').css('backgroundColor', $('#background_color').val());
    $('body').css('color', $('#text_color').val());
    $('body').css('fontSize', $('#text_size').val() + 'px');
}

